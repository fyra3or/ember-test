
Ember.Handlebars.registerBoundHelper('markdown', function(text) {
  return new Handlebars.SafeString(markdown.toHTML(text));
});

Ember.Handlebars.registerBoundHelper('money', function(value) {
  return accounting.formatMoney(value/100);
});
