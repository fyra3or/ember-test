
App.ProductView = Ember.View.extend({
  isOnSale: Ember.computed.alias('controller.isOnSale'),
  classNameBindings: 'isOnSale'
});

App.ReviewView = Ember.View.extend({
  isExpanded: false,
  readMore: Ember.computed.gt('length', 140),
  classNameBindings: ['isExpanded', 'readMore'],
  click: function() {
    this.toggleProperty('isExpanded')
  }
  // click is an ember view event: http://emberjs.com/api/classes/Ember.View.html
});

