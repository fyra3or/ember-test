
App.IndexController = Ember.ArrayController.extend({
  /*productsCount: function() {
    return this.get('length')
  }.property('length'),*/ // Shorthand below (Ember.computed.alias('length'))
  // Adding 'length' as a param to property,
  // will make the app listen to changes in length, and update content accordingly
  productsCount: Ember.computed.alias('length'),
  logo: 'images/logo-small.png',
  time: function() {
    return (new Date()).toDateString();
  }.property(),
  onSale: function() {
    return this.filterBy('isOnSale').slice(0,3);
  }.property('@each.isOnSale')
});

App.ContactsIndexController = Ember.ObjectController.extend({
  contactName: Ember.computed.alias('name'),
  avatar: 'images/contacts/anostagia.png',
  open: function() {
    return ((new Date()).getDay() === 0) ? "Closed" : "Open";
  }.property()
});

/*App.ProductsIndexController = Ember.ArrayController.extend({
  deals: function() {
    return this.filter(function(product) {
      return product.get('price') < 500;
    });
  }.property('@each.price')
});*/

App.ProductsController = Ember.ArrayController.extend({
  sortProperties: ['title']
});

App.ContactsController = Ember.ArrayController.extend({
  sortProperties: ['name']
});

App.ContactProductsController = Ember.ArrayController.extend({
  sortProperties: ['title']
});

App.ReviewsController = Ember.ArrayController.extend({
  sortProperties: ['reviewedAt'],
  sortAscending: false
});

App.ProductController = Ember.ObjectController.extend({
  ratings: [1,2,3,4,5],
  isNotReviewed: Ember.computed.alias('review.isNew'),
  review: function(){
    return this.store.createRecord('review',{
      product: this.get('model')
    });
  }.property('model'),
  actions: {
    createReview: function(){
      var controller = this;
      this.get('review').set('reviewedAt', new Date());
      this.get('review').save().then(function(review){
        controller.get('model.reviews').addObject(review);
      });
    }
  }
});
